** Program guide CZ**

Cílem projektu je vytvořit aplikaci, která bude zobrazovat denní EPG - programového průvodce  pro české TV stanice. Budou zde barevně odlišeny liché a sudé řádky. Buňky právě hrajících pořadů budou barevně zvýrazněny. Celý přehled bude překleslen čarou indikující aktuální čas. Data budou pravidelně aktualizována. Komponenta EPG by měla být dynamicky vykreslována adaptérem.

**Contributors**

* Dominik Hanák
* Šimon Hochla
* Martin Nekula


**Supervisor**

* RNDr. Bc. Jonáš Ševčík