/*
 * Copyright (C) 2012 Julien Vermet
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package epgcz.pv239.fi.muni.cz.epgcz.activity;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.lucasr.twowayview.widget.TwoWayView;

import java.util.ArrayList;
import java.util.List;

import epgcz.pv239.fi.muni.cz.epgcz.db.ChannelProvider;
import epgcz.pv239.fi.muni.cz.epgcz.db.EventProvider;
import epgcz.pv239.fi.muni.cz.epgcz.network.ChannelResponse;
import epgcz.pv239.fi.muni.cz.epgcz.network.ChannelService;
import epgcz.pv239.fi.muni.cz.epgcz.network.EventDescription;
import epgcz.pv239.fi.muni.cz.epgcz.network.EventResponse;
import epgcz.pv239.fi.muni.cz.epgcz.network.EventService;
import epgcz.pv239.fi.muni.cz.epgcz.network.HeaderAdapter;
import epgcz.pv239.fi.muni.cz.epgcz.tvprogram.TvProgram;
import fr.julienvermet.twodadapter.sample.epg.R;
import retrofit.RestAdapter;
import src.fr.julienvermet.twodadapter.twodadapter.TwoDAdapter;
import src.fr.julienvermet.twodadapter.twodadapter.TwoDElement;
import src.fr.julienvermet.twodadapter.twodadapter.widget.TwoDScrollView;


public class MainActivity extends Activity implements LoaderManager.LoaderCallbacks{

    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private static final long ONE_HOUR = 60*60;

    float mEpgOneHour;
    float mEpgElementHeight;
    long mXOrigin;

    private RelativeLayout mTwoDContent;
    private TwoDScrollView mTwoDScrollView;
    private ListView mHeaderLeft;
    private HorizontalScrollView mHeaderTop;

    private List<String> channelNames = new ArrayList<>();

    private SparseArray<ArrayList<TvProgram>> mTvChannels = new SparseArray<ArrayList<TvProgram>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getLoaderManager().initLoader(1, null, this);

        new AsyncTask<Void, Void, ChannelResponse>() {
            @Override
            protected ChannelResponse doInBackground(Void... params) {
                ChannelService channelService = initChannelService();
                ChannelResponse response = channelService.getChannels();
                ContentValues values = new ContentValues();
                for (String name: response.getNames()) {
                    values.put(ChannelProvider.Channel.NAME, name);
                    if (getApplicationContext().getContentResolver().
                            update(ChannelProvider.Channel.CONTENT_URI, values, null, null) == 0) {
                        getApplicationContext().getContentResolver().
                                insert(ChannelProvider.Channel.CONTENT_URI, values);
                    }
                }

                Cursor mChannelCursor = getApplicationContext().getContentResolver().query(ChannelProvider.Channel.CONTENT_URI, null, null, null, null);
                int index = mChannelCursor.getColumnIndex(ChannelProvider.Channel.NAME);

                if (null == mChannelCursor) {
                    android.util.Log.e(LOG_TAG, "Cursor is null.");
                } else {
                    while (mChannelCursor.moveToNext()) {
                        // Gets the value from the column.
                        String channelName = mChannelCursor.getString(index);
                        channelNames.add(channelName);
                    }
                }
                return response;
            }

            @Override
            protected void onPostExecute(ChannelResponse channelResponse) {
                Log.w("ChannelService", channelResponse.toString());
            }
        }.execute();



        new AsyncTask<Void, Void, EventResponse>() {
            @Override
            protected EventResponse doInBackground(Void... params) {
                EventService eventService = initEventService();
                EventResponse response = eventService.getEvents();
                ContentValues values = new ContentValues();
                if (response.getEventIDs() != null) {
                    for (String eventId : response.getEventIDs()) {
                        values.put(EventProvider.Event.EVENT_ID, eventId);
                        getApplicationContext().getContentResolver().insert(EventProvider.Event.CONTENT_URI, values);
                    }
                }
                return response;
            }

            @Override
            protected void onPostExecute(EventResponse eventResponse) {
                Log.w("EventService", eventResponse.toString());
            }
        }.execute();
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, ChannelProvider.Channel.CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
		/*-----LAYOUT handling-------*/

        mTwoDContent = (RelativeLayout) findViewById(R.id.twoDContent);
        mTwoDScrollView = (TwoDScrollView) findViewById(R.id.twoDScrollView);
        mHeaderTop = (HorizontalScrollView) findViewById(R.id.headerTop);
        mHeaderLeft = (ListView) findViewById(R.id.headerLeft);

        mHeaderLeft.setScrollContainer(false);
        mHeaderTop.setOnTouchListener(null);

        mHeaderLeft.setAdapter(new ArrayAdapter(this, R.layout.header_left_item, channelNames));


        mEpgOneHour = getResources().getDimension(R.dimen.epg_one_hour);
        mEpgElementHeight = getResources().getDimension(R.dimen.epg_element_height);

        mXOrigin = 1356631200;

        for (int i = 0; i < channelNames.size(); i++) {
            ArrayList<TvProgram> channel = new ArrayList<TvProgram>();
            long beginningTime = 1356631200;
            long endingTime = beginningTime + 4600 + 2000 % ((i + 1) * 200 );
            for (int j = 0; j < 8; j++) {
                TvProgram tvProgram1 = new TvProgram(beginningTime, endingTime, "Program "+ j);
                channel.add(tvProgram1);
                beginningTime = endingTime;
                endingTime = endingTime + 4600 + 2000 % ((i + 1) * 200 );
            }
            mTvChannels.put(i, channel);
        }

        new TwoDContentAdapter(mTwoDScrollView, mTwoDContent);
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    private ChannelService initChannelService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ChannelService.url).setLogLevel(RestAdapter.LogLevel.FULL).build();
        return restAdapter.create(ChannelService.class);
    }

    private EventService initEventService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(EventService.url).setLogLevel(RestAdapter.LogLevel.FULL).build();
        return restAdapter.create(EventService.class);
    }

    private ArrayList<Integer> getVisibleChannels(float y1, float y2) {
        ArrayList<Integer> visibleChannels = new ArrayList<Integer>();
        int channelY = 0;

        for (int i=0; i<mTvChannels.size(); i++) {
            if ((channelY >= y1 && channelY <= y2) || (channelY+mEpgElementHeight >= y1 && channelY+mEpgElementHeight <= y2)) {
                visibleChannels.add(i);
            }
            channelY += mEpgElementHeight;
        }
        return visibleChannels;
    }

    private float getProgramWidth(long startTime, long endTime) {
        double length = endTime - startTime;
        return (float) (length/ONE_HOUR)*mEpgOneHour;
    }

    private float getProgramX(long startTime) {
        return getProgramWidth(mXOrigin, startTime);
    }

    private class TwoDContentAdapter extends TwoDAdapter<TextView, TvProgram> {

        LayoutInflater mInflater;

        public TwoDContentAdapter(TwoDScrollView twoDScrollView, RelativeLayout twoDContent) {
            super(twoDScrollView, twoDContent);
            mInflater = getLayoutInflater();
        }

        @Override
        protected void bindView(TextView view, TvProgram data) {
            view.setText(data.name);
        }

        @Override
        protected TextView newView(boolean even) {
            if (even) {
                return (TextView) mInflater.inflate(R.layout.epg_element_even, null);
            } else {
                return (TextView) mInflater.inflate(R.layout.epg_element_odd, null);
            }
        }

        @Override
        protected ArrayList<TwoDElement<TvProgram>> getElements(float scrollLeft,
                                                                float scrollRight, float scrollTop, float scrollBottom) {

            ArrayList<TwoDElement<TvProgram>> mElements = new ArrayList<TwoDElement<TvProgram>>();

            ArrayList<Integer> visibleChannels = getVisibleChannels(scrollTop, scrollBottom);
            for (int channel : visibleChannels) {
                float channelY = (int) (channel * mEpgElementHeight);
                ArrayList<TvProgram> tvPrograms = mTvChannels.get(channel);
                for (TvProgram tvProgram : tvPrograms) {
                    float programX = getProgramX(tvProgram.startTime);
                    float programWidth = getProgramWidth(tvProgram.startTime, tvProgram.endTime);
                    TwoDElement<TvProgram> element = new TwoDElement<TvProgram>(programX, channelY, programWidth, mEpgElementHeight, tvProgram);
                    mElements.add(element);
                }
            }
            return mElements;
        }

        @Override
        public void onScrollChanged(int x, int y, int oldx, int oldy) {
            super.onScrollChanged(x, y, oldx, oldy);

            if (x == oldx) {
                mHeaderLeft.scrollListBy(y - oldy);
            } else if (y == oldy) {
                mHeaderTop.scrollTo(x, mHeaderTop.getScrollY());
            } else {
                mHeaderLeft.scrollListBy(y - oldy);
                mHeaderTop.scrollTo(x, mHeaderTop.getScrollY());
            }
        }
    }
}