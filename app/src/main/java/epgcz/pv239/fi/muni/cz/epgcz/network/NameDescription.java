package epgcz.pv239.fi.muni.cz.epgcz.network;

/**
 * Created by Simon on 3. 6. 2015.
 */
public class NameDescription {
    private String name;

    @Override
    public String toString() {
        return "NameDescription{" +
                "name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }
}
