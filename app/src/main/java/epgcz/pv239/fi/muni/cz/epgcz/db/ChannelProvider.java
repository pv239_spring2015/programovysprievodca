package epgcz.pv239.fi.muni.cz.epgcz.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;

import java.util.HashMap;

/**
 * Created by michal on 20.4.2015.
 */
public class ChannelProvider extends ContentProvider {
    private static final String AUTHORITY = "epgcz.pv239.fi.muni.cz.epgcz";

    private static UriMatcher uriMatcher = null;
    private static HashMap<String, String> channelMap;

    public static final int CHANNEL = 1;
    public static final int CHANNEL_ID = 2;

    public static final class Channel implements BaseColumns {

        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/channel");
        public static final String CONTENT_TYPE = "vnd.android.dir/vnd." + AUTHORITY + "." + DBHelper.DATABASE_CHANNEL_TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.item/vnd." + AUTHORITY + "." + DBHelper.DATABASE_CHANNEL_TABLE_NAME;


        public static final String NAME = "name";
    }

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, DBHelper.DATABASE_CHANNEL_TABLE_NAME, CHANNEL);
        uriMatcher.addURI(AUTHORITY, DBHelper.DATABASE_CHANNEL_TABLE_NAME + "/#", CHANNEL_ID);

        channelMap = new HashMap<>();
        channelMap.put(Channel._ID, DBHelper.DATABASE_CHANNEL_TABLE_NAME + "." + Channel._ID);
        channelMap.put(Channel.NAME, DBHelper.DATABASE_CHANNEL_TABLE_NAME + "." + Channel.NAME);
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case CHANNEL:
                return Channel.CONTENT_TYPE;
            case CHANNEL_ID:
                return Channel.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    private DBHelper dbHelper;

    @Override
    public boolean onCreate() {
        dbHelper = new DBHelper(getContext(), DBHelper.DATABASE_NAME, null, DBHelper.DATABASE_VERSION);
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase database = dbHelper.getReadableDatabase();

        Cursor result;

        switch (uriMatcher.match(uri)) {
            case CHANNEL:
                result = database.query(DBHelper.DATABASE_CHANNEL_TABLE_NAME, projection, selection,
                        selectionArgs, sortOrder, null, null);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        if (result != null) {
            result.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return result;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        long rowID = -1;
        Uri contentUri = null;

        switch (uriMatcher.match(uri)) {
            case CHANNEL:
                rowID = database.insertWithOnConflict(DBHelper.DATABASE_CHANNEL_TABLE_NAME, null, values, 0);
                contentUri = Channel.CONTENT_URI;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        if (rowID != -1) {
            Uri noteUri = ContentUris.withAppendedId(contentUri, rowID);
            getContext().getContentResolver().notifyChange(noteUri, null);
            return noteUri;
        }

        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

}