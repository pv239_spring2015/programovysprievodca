package epgcz.pv239.fi.muni.cz.epgcz.network;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.lucasr.twowayview.widget.TwoWayView;

import java.util.ArrayList;
import java.util.List;

import epgcz.pv239.fi.muni.cz.epgcz.model.HeaderTime;
import fr.julienvermet.twodadapter.sample.epg.R;

/**
 * Created by dhanak on 7/14/15.
 */
public class HeaderAdapter extends RecyclerView.Adapter<HeaderAdapter.SimpleViewHolder> {

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.header_top_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.time.setText(mItems.get(position).hour);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        public final TextView time;

        public SimpleViewHolder(View view) {
            super(view);
            time = (TextView) view.findViewById(R.id.time);
        }
    }

    private final Context mContext;
    private final TwoWayView mRecyclerView;
    private final List<HeaderTime> mItems;

    public HeaderAdapter(Context context, TwoWayView recyclerView) {
        super();
        mContext = context;
        mItems = new ArrayList<HeaderTime>();

        for (int i = 0; i < 24; i++) {
            HeaderTime ht = new HeaderTime(String.valueOf(i));
            mItems.add(ht);
        }
        mRecyclerView = recyclerView;
    }
}
