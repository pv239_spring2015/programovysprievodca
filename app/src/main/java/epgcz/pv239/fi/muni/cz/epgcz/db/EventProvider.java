package epgcz.pv239.fi.muni.cz.epgcz.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;

import java.util.HashMap;

/**
 * Created by michal on 20.4.2015.
 */
public class EventProvider extends ContentProvider {
    private static final String AUTHORITY = "epgcz.pv239.fi.muni.cz.epgcz";

    private static UriMatcher uriMatcher = null;
    private static HashMap<String, String> eventMap;

    public static final int EVENT = 1;
    public static final int EVENT_ID = 2;

    public static final class Event implements BaseColumns {

        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/event");
        public static final String CONTENT_TYPE = "vnd.android.dir/vnd." + AUTHORITY + "." + DBHelper.DATABASE_EVENT_TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.item/vnd." + AUTHORITY + "." + DBHelper.DATABASE_EVENT_TABLE_NAME;


        public static final String EVENT_ID = "eventId";
        public static final String TITLE = "title";
        public static final String START_TIME = "startTime";
        public static final String END_TIME = "endTime";
        public static final String CHANNEL = "channel";
        public static final String event = "event";
    }

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, DBHelper.DATABASE_EVENT_TABLE_NAME, EVENT);
        uriMatcher.addURI(AUTHORITY, DBHelper.DATABASE_EVENT_TABLE_NAME + "/#", EVENT_ID);

        eventMap = new HashMap<>();
        eventMap.put(Event._ID, DBHelper.DATABASE_EVENT_TABLE_NAME + "." + Event._ID);
        eventMap.put(Event.EVENT_ID, DBHelper.DATABASE_EVENT_TABLE_NAME + "." + Event.EVENT_ID);
        eventMap.put(Event.TITLE, DBHelper.DATABASE_EVENT_TABLE_NAME + "." + Event.TITLE);
        eventMap.put(Event.START_TIME, DBHelper.DATABASE_EVENT_TABLE_NAME + "." + Event.START_TIME);
        eventMap.put(Event.END_TIME, DBHelper.DATABASE_EVENT_TABLE_NAME + "." + Event.END_TIME);
        eventMap.put(Event.CHANNEL, DBHelper.DATABASE_EVENT_TABLE_NAME + "." + Event.CHANNEL);
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case EVENT:
                return Event.CONTENT_TYPE;
            case EVENT_ID:
                return Event.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    private DBHelper dbHelper;

    @Override
    public boolean onCreate() {
        dbHelper = new DBHelper(getContext(), DBHelper.DATABASE_NAME, null, DBHelper.DATABASE_VERSION);
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase database = dbHelper.getReadableDatabase();

        Cursor result;

        switch (uriMatcher.match(uri)) {
            case EVENT:
                result = database.query(DBHelper.DATABASE_EVENT_TABLE_NAME, projection, selection,
                        selectionArgs, sortOrder, null, null);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        if (result != null) {
            result.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return result;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        long rowID = -1;
        Uri contentUri = null;

        switch (uriMatcher.match(uri)) {
            case EVENT:
                rowID = database.insertWithOnConflict(DBHelper.DATABASE_EVENT_TABLE_NAME, null, values, 0);
                contentUri = Event.CONTENT_URI;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        if (rowID != -1) {
            Uri noteUri = ContentUris.withAppendedId(contentUri, rowID);
            getContext().getContentResolver().notifyChange(noteUri, null);
            return noteUri;
        }

        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

}