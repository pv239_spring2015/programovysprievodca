package epgcz.pv239.fi.muni.cz.epgcz.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Simon on 31. 5. 2015.
 */
public class DBHelper extends SQLiteOpenHelper {
    public final static String DATABASE_NAME = "epg";
    public final static int DATABASE_VERSION = 1;
    public final static String DATABASE_CHANNEL_TABLE_NAME = "channel";
    public final static String DATABASE_EVENT_TABLE_NAME = "event";

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuffer buffer1 = new StringBuffer();
        buffer1.append("CREATE TABLE ");
        buffer1.append(DATABASE_CHANNEL_TABLE_NAME);
        buffer1.append(" (");
        buffer1.append(ChannelProvider.Channel._ID);
        buffer1.append(" INTEGER PRIMARY KEY AUTOINCREMENT,");
        buffer1.append(ChannelProvider.Channel.NAME);
        buffer1.append(" TEXT");
        buffer1.append(")");
        db.execSQL(buffer1.toString());

//        StringBuffer buffer2 = new StringBuffer();
//        buffer2.append("CREATE TABLE ");
//        buffer2.append(DATABASE_EVENT_TABLE_NAME);
//        buffer2.append(" (");
//        buffer2.append(ChannelProvider.Forecast._ID);
//        buffer2.append(" INTEGER PRIMARY KEY AUTOINCREMENT,");
//        buffer2.append(ChannelProvider.Forecast.DESCRIPTION);
//        buffer2.append(" TEXT,");
//        buffer2.append(ChannelProvider.Forecast.TEMPERATURE);
//        buffer2.append(" REAL");
//        buffer2.append(")");
//        db.execSQL(buffer2.toString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
