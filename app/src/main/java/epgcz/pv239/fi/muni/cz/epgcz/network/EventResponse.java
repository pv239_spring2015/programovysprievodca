package epgcz.pv239.fi.muni.cz.epgcz.network;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class EventResponse {
    @SerializedName("ct1")
    private List<EventDescription> descriptions;

    @Override
    public String toString() {
        return "EventResponse{" +
                "descriptions=" + descriptions +
                '}';
    }

    public List<String> getEventIDs() {
        List<String> eventIds = new ArrayList<>();
        if (descriptions == null) return null;
        for (EventDescription eventDescription: descriptions) {
            eventIds.add(eventDescription.getEventId());
        }
        return eventIds;
    }
}
