package epgcz.pv239.fi.muni.cz.epgcz.network;

import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Simon on 31. 5. 2015.
 */
public interface ChannelService {
    static String url = "http://private-a97f7-epg2.apiary-mock.com";
    static String NAME = "ChannelService";

    @GET("/playlist")
    ChannelResponse getChannels();
}
