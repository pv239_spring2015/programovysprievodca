package epgcz.pv239.fi.muni.cz.epgcz.network;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Simon on 31. 5. 2015.
 */
public class ChannelResponse {
    @SerializedName("channels")
    private List<NameDescription> names;


    @Override
    public String toString() {
        return "ChannelResponse{" +
                "names=" + names +
                '}';
    }

    public List<String> getNames() {
        List<String> nameStrings = new ArrayList<>();
        for (NameDescription nameDesc: names) {
            nameStrings.add(nameDesc.getName());
        }
        return nameStrings;
    }
}
