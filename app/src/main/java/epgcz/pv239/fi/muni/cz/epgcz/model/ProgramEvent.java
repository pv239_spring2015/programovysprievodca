package epgcz.pv239.fi.muni.cz.epgcz.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by dhanak on 5/18/15.
 */
public class ProgramEvent implements Parcelable {
    private String title;
    private String startTime;
    private String endTime;
    private String channel;

    public ProgramEvent(String title, String startTime, String endTime, String channel) {
        this.title = title;
        this.startTime = startTime;
        this.endTime = endTime;
        this.channel = channel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
