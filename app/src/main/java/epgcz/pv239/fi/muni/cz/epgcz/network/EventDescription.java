package epgcz.pv239.fi.muni.cz.epgcz.network;

/**
 * Created by Simon on 15. 7. 2015.
 */
public class EventDescription {
    private String eventId;

    private String title;

    private String startTime;

    private String endTime;

    private String channel;

    public String getEventId() {
        return eventId;
    }

    public String getTitle() {
        return title;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getChannel() {
        return channel;
    }

    @Override
    public String toString() {
        return "EventResponse{" +
                "eventId='" + eventId + '\'' +
                ", title='" + title + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", channel='" + channel + '\'' +
                '}';
    }
}
