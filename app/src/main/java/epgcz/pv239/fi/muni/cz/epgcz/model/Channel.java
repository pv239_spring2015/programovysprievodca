package epgcz.pv239.fi.muni.cz.epgcz.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dhanak on 4/29/15.
 */
public class Channel implements Parcelable{

    private String name;
    private List<ProgramEvent> programs;

    public Channel() {
        programs = new ArrayList<>();
    }

    public Channel(String name) {
        programs = new ArrayList<>();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProgramEvent> getPrograms() {
        return programs;
    }

    public void setPrograms(List<ProgramEvent> programs) {
        this.programs = programs;
    }

    public void addCollection(List<ProgramEvent> list) {
        programs.addAll(list);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
